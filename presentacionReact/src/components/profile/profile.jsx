import './profile.css';
import myPhoto from '../../assets/photo.jpeg';
import reactLogo from '../../assets/react.svg';
import facebook from '../../assets/facebook.png';
import instagram from '../../assets/instagram.png';
import github from '../../assets/github.png';
import linkedin from '../../assets/linkedin.png';
import microsoft from '../../assets/microsoft.png';
export const Profile = () =>{

    return(
        <div className="principal-container">
            <div className="avatar-container">
                <div className="avatar-container__myphoto">
                    <img src={myPhoto} alt=""/>
                </div>
                <div className="avatar-container__framework">
                    <img src={reactLogo} alt=""/>
                </div>
            </div>

            <div className="social-media">
                <a href="" target="_blank"><img src={facebook} alt="" className="social-media-icon"/></a>
                <a href=""><img src={instagram} alt="" className="social-media-icon"/></a>
                <a href="https://github.com/AlvaroTorres0?tab=stars" target='_blank'><img src={github} alt="" className="social-media-icon"/></a>
                <a href="https://www.linkedin.com/in/alvaro-isaias-espindola-torres" target="_blank"><img src={linkedin} alt="" className="social-media-icon"/></a>
                <a href="https://learn.microsoft.com/es-es/users/alvarotorres/" target="_blank"><img src={microsoft} alt="" className="social-media-icon"/></a>
            </div>

            <div className="container-name">
                <h1>Álvaro Isaías Espíndola Torres</h1>
            </div>
        </div>
    )
}
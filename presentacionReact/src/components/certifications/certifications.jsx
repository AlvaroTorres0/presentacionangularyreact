import './certifications.css';
import css from '../../assets/Certifications/BootcampCSS.jpg';
import azure from '../../assets/Certifications/Azure.jpg';
import dom from '../../assets/Certifications/DOM.jpg';
import webApis from '../../assets/Certifications/WebAPIS.jpg';

export const Certifications = () =>{
    return(
        <div class="container-certifications">
            <h1>Certificaciones y cursos</h1>
            <div className='certifications'>
                <div class="certifications-item">
                    <div class="card mb-3 card-item">
                        <div class="card-img">
                            <img src={css} class="card-img-top" alt="..."/>
                        </div>
                        
                        <div class="card-body">
                        <h5 class="card-title">Fundamentos de CSS</h5>
                        <p class="card-text" id="descripcion">Principios básicos, estructuras, selectores, propiedades de caja, posicionamiento, colorización, Flex-box y GRID.</p>
                        <p class="card-text"><small class="text-muted">Noviembre 2022</small></p>
                        </div>
                    </div>
                </div>

                <div class="certifications-item">
                    <div class="card mb-3 card-item">
                        <div class="card-img">
                            <img src={azure} class="card-img-top" alt="..."/>
                        </div>
                        
                        <div class="card-body">
                        <h5 class="card-title">Azure Fundamentals (AZ-900)</h5>
                        <p class="card-text">Servicios básicos, soluciones y herramientos, seguridad general y de red, identidad, gobernanza, privacidad y complimiento y precios.</p>
                        <p class="card-text"><small class="text-muted">Noviembre 2021</small></p>
                        </div>
                    </div>
                </div>

                <div class="certifications-item">
                    <div class="card mb-3 card-item">
                        <div class="card-img">
                            <img src={dom} class="card-img-top" alt="..."/>
                        </div>
                        
                        <div class="card-body">
                        <h5 class="card-title">JavaScript y el DOM</h5>
                        <p class="card-text">Variable, tipos de datos, ciclos y condicionales, arrays,POO, métodos de cadena, métodos de arrays, manejo de consola y el DOM.</p>
                        <p class="card-text"><small class="text-muted">Noviembre 2022</small></p>
                        </div>
                    </div>
                </div>

                <div class="certifications-item">
                    <div class="card mb-3 card-item">
                        <div class="card-img">
                            <img src={webApis} class="card-img-top" alt="..."/>
                        </div>
                        
                        <div class="card-body">
                        <h5 class="card-title">Web API´s</h5>
                        <p class="card-text">Creación y consumo de una API REST con JavaScript, Fetch, Asincronía (setTimeout), CallBacks a profundidad y promesas.</p>
                        <p class="card-text"><small class="text-muted">Noviembre 2022</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
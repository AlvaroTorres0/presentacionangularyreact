import './App.css';
import { Profile } from './components/profile/profile';
import { Cards } from './components/cards/cards';
import { Certifications } from './components/certifications/certifications'

function App() {

  return (
    <div className='container-principal'>
      <div className='container__profile'>
        <Profile></Profile>
      </div>
      <div className='container__cards'>
        <Cards></Cards>
      </div>
      <div className='container__certifications'>
        <Certifications></Certifications>
      </div>
    </div>
  )
}

export default App